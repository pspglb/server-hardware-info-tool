# server-hardware-info-tool
1. 通过HP服务器 ILO4 Web接口获取服务器硬件信息,目前包涵CPU信息，内存信息，网卡信息
2. 支持swagger
3. 此处为单点开源获取服务器信息版本，适用于批量获取小规模服务器信息取(几十台，几百台)，作为一个简单查阅服务器硬件的服务
4. 数量级过大的，真实场景是上千台服务器，则是通过kafka作为消息中间件，将该工具部署至k8s，通过kafka读取消息，到服务器获取信息后回写消息队列，再写入redis和postgres，**==此处版本不支持==**

## Install

```
  go get https://gitee.com/pippozq/server-hardware-info-tool.git
```
## Build

```
go build -o server_info && ./server_info

```

## Swagger

http://127.0.0.1:8080/swagger

## Get Server Infomation

data.json

```
{
"hosts":["172.16.250.20","172.16.250.21"],
"user":{
    "UserName":"admin",
    "Password":"password"
    }
}

```
url: http://127.0.0.1:8080/v1.0.0/hp/all

```
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \
 "hosts":["172.16.250.20","172.16.250.21"], \
 "user":{ \
     "UserName":"admin", \
     "Password":"password" \
     } \
 }' 'http://127.0.0.1:8080/v1.0.0/hp/all'

```

Response

```
{
  "status": 0,
  "msg": [
    {
      "host": "172.16.250.20",
      "user": {
        "UserName": "admin",
        "Password": "password"
      },
      "memory": [
        {
          "Id": "proc2dimm12",
          "Name": "proc2dimm12",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 2 DIMM 12"
        },
        {
          "Id": "proc1dimm4",
          "Name": "proc1dimm4",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 1 DIMM 4"
        },
        {
          "Id": "proc1dimm12",
          "Name": "proc1dimm12",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 1 DIMM 12"
        },
        {
          "Id": "proc2dimm4",
          "Name": "proc2dimm4",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 2 DIMM 4"
        },
        {
          "Id": "proc2dimm1",
          "Name": "proc2dimm1",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 2 DIMM 1"
        },
        {
          "Id": "proc1dimm9",
          "Name": "proc1dimm9",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 1 DIMM 9"
        },
        {
          "Id": "proc2dimm9",
          "Name": "proc2dimm9",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 2 DIMM 9"
        },
        {
          "Id": "proc1dimm1",
          "Name": "proc1dimm1",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 1 DIMM 1"
        }
      ],
      "cpu": {
        "ProcessorFamily": "Intel(R) Xeon(R) CPU E5-2640 v3 @ 2.60GHz",
        "Count": 2,
        "Status": "OK"
      },
      "network_adapter": [
        {
          "Name": "HP Ethernet 1Gb 4-port 331i Adapter - NIC",
          "PhysicalPorts": [
            {
              "FullDuplex": false,
              "MacAddress": "1C:98:EC:2F:AC:74",
              "GoodReceives": 0,
              "GoodTransmits": 0,
              "StructuredName": "",
              "Type": ""
            },
            {
              "FullDuplex": false,
              "MacAddress": "1C:98:EC:2F:AC:75",
              "GoodReceives": 0,
              "GoodTransmits": 0,
              "StructuredName": "",
              "Type": ""
            },
            {
              "FullDuplex": false,
              "MacAddress": "1C:98:EC:2F:AC:76",
              "GoodReceives": 0,
              "GoodTransmits": 0,
              "StructuredName": "",
              "Type": ""
            },
            {
              "FullDuplex": false,
              "MacAddress": "1C:98:EC:2F:AC:77",
              "GoodReceives": 0,
              "GoodTransmits": 0,
              "StructuredName": "",
              "Type": ""
            }
          ]
        }
      ],
      "status": "Ok"
    },
    {
      "host": "172.16.250.21",
      "user": {
        "UserName": "admin",
        "Password": "password"
      },
      "memory": [
        {
          "Id": "proc1dimm12",
          "Name": "proc1dimm12",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 1 DIMM 12"
        },
        {
          "Id": "proc2dimm12",
          "Name": "proc2dimm12",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 2 DIMM 12"
        },
        {
          "Id": "proc2dimm4",
          "Name": "proc2dimm4",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 2 DIMM 4"
        },
        {
          "Id": "proc1dimm4",
          "Name": "proc1dimm4",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 1 DIMM 4"
        },
        {
          "Id": "proc2dimm9",
          "Name": "proc2dimm9",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 2 DIMM 9"
        },
        {
          "Id": "proc1dimm1",
          "Name": "proc1dimm1",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 1 DIMM 1"
        },
        {
          "Id": "proc2dimm1",
          "Name": "proc2dimm1",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 2 DIMM 1"
        },
        {
          "Id": "proc1dimm9",
          "Name": "proc1dimm9",
          "Type": "HpMemory.1.0.0",
          "DIMMStatus": "GoodInUse",
          "DIMMTechnology": "RDIMM",
          "DIMMType": "DDR4",
          "DataWidth": 64,
          "TotalWidth": 72,
          "HPMemoryType": "HPSmartMemory",
          "Manufacturer": "HP     ",
          "MaximumFrequencyMHz": 2133,
          "MinimumVoltageVoltsX10": 12,
          "PartNumber": "752369-081",
          "Rank": 2,
          "SizeMB": 16384,
          "SocketLocator": "PROC 1 DIMM 9"
        }
      ],
      "cpu": {
        "ProcessorFamily": "Intel(R) Xeon(R) CPU E5-2640 v3 @ 2.60GHz",
        "Count": 2,
        "Status": "OK"
      },
      "network_adapter": [
        {
          "Name": "HP Ethernet 1Gb 4-port 331i Adapter - NIC",
          "PhysicalPorts": [
            {
              "FullDuplex": false,
              "MacAddress": "1C:98:EC:2F:32:F4",
              "GoodReceives": 0,
              "GoodTransmits": 0,
              "StructuredName": "",
              "Type": ""
            },
            {
              "FullDuplex": false,
              "MacAddress": "1C:98:EC:2F:32:F5",
              "GoodReceives": 0,
              "GoodTransmits": 0,
              "StructuredName": "",
              "Type": ""
            },
            {
              "FullDuplex": false,
              "MacAddress": "1C:98:EC:2F:32:F6",
              "GoodReceives": 0,
              "GoodTransmits": 0,
              "StructuredName": "",
              "Type": ""
            },
            {
              "FullDuplex": false,
              "MacAddress": "1C:98:EC:2F:32:F7",
              "GoodReceives": 0,
              "GoodTransmits": 0,
              "StructuredName": "",
              "Type": ""
            }
          ]
        }
      ],
      "status": "Ok"
    }
  ]
}
```


## License
source code is licensed under the Apache Licence, Version 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html).


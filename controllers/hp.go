package controllers

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"gitee.com/pippozq/server-hardware-info-tool/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/httplib"
	"github.com/bitly/go-simplejson"
	"time"
)

type HpController struct {
	BaseController
}

type HpIlo struct {
	models.HpServer
	Status string `json:"status"`
}

type HP interface {
	GetSession() error
	DelSession() error
	GetReq(url string) (req *httplib.BeegoHTTPRequest)
	// Processor
	GetProcessor() (processors models.Processors, err error)
	// NetworkAdapter
	GetMacAddresses() (macAddr []models.NetworkAdapter, err error)
	GetMacAddress(url string, macChan chan models.NetworkAdapter) (err error)
	// Memory
	GetMemories() (memories []models.Memory, err error)
	GetMemory(url string, memChan chan models.Memory) (err error)
}

// ILO URL
const AUTHURL = "redfish/v1/sessions/"
const SESSTIONLOCATION = "Location"
const SESSIONXAUTHTOKEN = "X-Auth-Token"

// Url
const MACADDR = "rest/v1/Systems/1/NetworkAdapters"
const MEMORY = "rest/v1/Systems/1/Memory"
const DETAIL = "rest/v1/Systems/1"

// @Title  Get all information including cpu, memory, network adapter
// @Description Get HP Server
// @Param   post_body body  string  true "{"hosts":'[host1,host2,host3]',"user": {'UserName':'name','Password':'password'}}"
// @Success 200 {object} models.HpServer
// @Failure 404 body is empty
// @router /all [post]
func (hp *HpController) GetAll() {

	rec := new(models.HpReceive)

	err := json.Unmarshal(hp.Ctx.Input.RequestBody, &rec)

	var serverInfo []HpIlo

	if err == nil {

		hpChan := make(chan *HpIlo)

		for _, v := range rec.Hosts {

			hpServer := new(HpIlo)
			hpServer.User = rec.User
			hpServer.Host = v

			go hp.GetHpServerInfo(hpServer, "all", hpChan)
		}

		for {
			v, isClose := <-hpChan
			if !isClose {
				break
			}

			serverInfo = append(serverInfo, *v)

			if len(serverInfo) == len(rec.Hosts) {
				close(hpChan)
			}
		}

	}
	hp.ReturnJson(0, serverInfo)
}

func (hp *HpController) GetHpServerInfo(ilo *HpIlo, info string, server chan *HpIlo) {
	ilo.Status = "Ok"
	err := ilo.GetSession()
	if err == nil{
		if info == "network" {
			hp.GetNetworkAdapter(ilo)
		} else if info == "cpu" {
			hp.GetCpu(ilo)
		} else if info == "memory" {
			hp.GetMemory(ilo)
		} else if info == "all" {
			hp.GetNetworkAdapter(ilo)
			hp.GetCpu(ilo)
			hp.GetMemory(ilo)
		}
		defer ilo.DelSession()
	}


	server <- ilo

}

func (hp *HpController) GetNetworkAdapter(hps *HpIlo) (err error) {
	hps.NetAdapter, err = hps.GetMacAddresses()
	return err
}

func (hp *HpController) GetCpu(hps *HpIlo) (err error) {

	hps.Cpu, err = hps.GetProcessor()
	return err
}

func (hp *HpController) GetMemory(hps *HpIlo) (err error) {
	hps.Mem, err = hps.GetMemories()
	return err
}

func (hp *HpIlo) GetSession() error {
	req := httplib.Post(fmt.Sprintf("https://%s/%s", hp.Host, AUTHURL))
	req.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	req.JSONBody(hp.User)
	response, err := req.SetTimeout(6 * time.Second, 15 * time.Second).Response()
	if err != nil {
		beego.Error(err)
		hp.Status = err.Error()
		return err
	} else {
		if response.StatusCode == 201 {
			hp.Session.Location = response.Header.Get(SESSTIONLOCATION)
			hp.Session.AuthToken = response.Header.Get(SESSIONXAUTHTOKEN)
		}
	}

	return nil
}

func (hp *HpIlo) DelSession() error {
	req := httplib.Delete(hp.Session.Location)
	_, err := req.Response()
	if err != nil {
		return err
	}
	return nil
}

// Create Req
func (hp *HpIlo) GetReq(url string) (req *httplib.BeegoHTTPRequest) {
	req = httplib.Get(url)
	req.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	req.Header(SESSTIONLOCATION, hp.Session.Location)
	req.Header(SESSIONXAUTHTOKEN, hp.Session.AuthToken)
	req.SetTimeout(6 * time.Second, 15 * time.Second)
	return req
}

// Get Mac Address List
func (hp *HpIlo) GetMacAddresses() (macAddr []models.NetworkAdapter, err error) {
	getMacUrl := fmt.Sprintf("https://%s/%s", hp.Host, MACADDR)
	req := hp.GetReq(getMacUrl)
	resp, err := req.Response()
	if resp.StatusCode == 200 {
		macByte, byteErr := req.Bytes()
		if byteErr == nil {
			macJson, _ := simplejson.NewJson([]byte(macByte))
			macAddresses, _ := macJson.GetPath("links", "Member").Array()
			var macs []string
			for _, v := range macAddresses {
				macs = append(macs, v.(map[string]interface{})["href"].(string))
			}
			var macChan = make(chan models.NetworkAdapter, 10)
			// Get Every Memory Info
			for _, v := range macs {
				go hp.GetMacAddress(v, macChan)
			}
			for {
				v, isClose := <-macChan
				if !isClose {
					break
				}
				macAddr = append(macAddr, v)

				if len(macAddr) == len(macs) {
					close(macChan)
				}
			}
		}
	}
	return macAddr, err
}

func (hp *HpIlo) GetMacAddress(url string, macChan chan models.NetworkAdapter) (err error) {
	getMacUrl := fmt.Sprintf("https://%s%s", hp.Host, url)
	req := hp.GetReq(getMacUrl)
	resp, err := req.Response()
	var networkAdapter models.NetworkAdapter
	if resp.StatusCode == 200 {
		macByte, err := req.Bytes()
		err = json.Unmarshal(macByte, &networkAdapter)

		if err == nil {
			macChan <- networkAdapter
		} else {
			beego.Error(err)
		}
	}
	return err
}

// Get single memory
func (hp *HpIlo) GetMemory(url string, memChan chan models.Memory) (err error) {

	getMacUrl := fmt.Sprintf("https://%s%s", hp.Host, url)
	req := hp.GetReq(getMacUrl)
	resp, err := req.Response()
	var memory models.Memory
	if resp.StatusCode == 200 {
		memByte, err := req.Bytes()
		err = json.Unmarshal(memByte, &memory)

		if err == nil {
			memChan <- memory
		} else {
			beego.Error(err)
		}

	}
	return err
}

// Get memory list
func (hp *HpIlo) GetMemories() (memories []models.Memory, err error) {
	getMacUrl := fmt.Sprintf("https://%s/%s", hp.Host, MEMORY)
	req := hp.GetReq(getMacUrl)
	resp, err := req.Response()
	if resp.StatusCode == 200 {
		macByte, byteErr := req.Bytes()
		if byteErr == nil {
			memoryJson, _ := simplejson.NewJson([]byte(macByte))
			mAddr, _ := memoryJson.GetPath("links", "Member").Array()
			// Get Memory List
			var mems []string
			for _, v := range mAddr {
				mems = append(mems, v.(map[string]interface{})["href"].(string))
			}
			var memoryChan = make(chan models.Memory, 10)
			// Get Every Memory Info
			for _, v := range mems {
				go hp.GetMemory(v, memoryChan)
			}
			for {
				v, isClose := <-memoryChan
				if !isClose {
					break
				}

				memories = append(memories, v)
				if len(memories) == len(mems) {
					close(memoryChan)
				}
			}
		}
	}
	return memories, err
}

// Get Processor
func (hp *HpIlo) GetProcessor() (processors models.Processors, err error) {
	url := fmt.Sprintf("https://%s/%s", hp.Host, DETAIL)
	req := hp.GetReq(url)
	resp, err := req.Response()

	if resp.StatusCode == 200 {

		processorByte, err := req.Bytes()
		if err == nil {
			processorJson, _ := simplejson.NewJson([]byte(processorByte))
			processor, _ := processorJson.GetPath("Processors").Map()
			processors.Status = processor["Status"].(map[string]interface{})["HealthRollUp"].(string)
			processors.ProcessorFamily = processor["ProcessorFamily"].(string)
			processors.Count, _ = processor["Count"].(json.Number).Int64()
		}

	}
	return processors, err
}

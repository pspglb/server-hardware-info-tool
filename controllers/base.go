package controllers

import "github.com/astaxie/beego"

type BaseController struct {
	beego.Controller
}

type ReturnJson struct {
	Status int         `json:"status"`
	Msg    interface{} `json:"msg"`
}

func (base *BaseController) ReturnJson(status int, msg interface{}) {
	base.Data["json"] = ReturnJson{
		Status: status,
		Msg:    msg,
	}
	base.ServeJSON()
}
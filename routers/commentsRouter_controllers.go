package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["gitee.com/pippozq/server-hardware-info-tool/controllers:HpController"] = append(beego.GlobalControllerRouter["gitee.com/pippozq/server-hardware-info-tool/controllers:HpController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/all`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

}
